package checkmobile.de.hertz;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import checkmobile.de.hertz.upload.UploadBean;
import checkmobile.de.hertz.upload.types.UploadContainer;
import checkmobile.de.hertz.upload.types.UploadUser;
import checkmobile.de.hertz.view.DrawView;

@EActivity(R.layout.activity_driver_signature)
public class DriverSignature extends AppCompatActivity {

	@Bean
	UploadBean uploadBean;

	@ViewById(R.id.drawView)
	DrawView drawView;

	@Click(R.id.acceptButton)
	void accept() {
		Intent intent = DriverSignature.this.getIntent();
		DriverSignature.this.setResult(RESULT_OK, intent);
		String signatureBase64 = drawView.getAsBase64();
		RestTest rt = new RestTest(signatureBase64);
		rt.execute();
		finish();
	}

	@Click(R.id.buttonReset)
	void reset() {
		drawView.reset();
	}

	class RestTest extends AsyncTask<Void, Void, Void> {

		private Exception exception;

		private String signature;

		public RestTest(String signature) {
			this.signature = signature;
		}

		protected Void doInBackground(Void... urls) {

			// String url = "http://192.168.81.24:8081/api/v1/infleeting";

			String url = "http://10.0.29.16/api/v1/infleeting";

			try {
				String json = "{\"deliveryNote\":{\"deliveryNumber\":\"123\",\"licensePlate\":\"31\",\"driverName\":\"123\",\"driverEmail\":\"312@mail.com\",\"vehicleCount\":2,\"condition\":\"ok\"},\"vehicles\":[{\"order\":0,\"vehicleCoreData\":{\"unitNumber\":123,\"licensePlate\":\"31\",\"vin\":123,\"carGroup\":\"A\"},\"mileage\":{\"mileage\":11},\"fueling\":{\"fuel\":2},\"damageConditions\":{\"damages\":[{\"area\":\"Front\",\"piece\":\"Bumper\",\"type\":\"Crack\",\"severity\":\"Small\",\"pictures\":[]}]},\"vehicleAdditionalData\":{\"vehicleType\":\"PKW\",\"model\":\"Q5\",\"manufacturer\":\"Audi\",\"gearing\":\"automatic\",\"fuelType\":\"Diesel\",\"tankCapacity\":22,\"сolour\":\"black\",\"metallic\":true},\"overviewPhoto\":{\"pictures\":[]},\"vehicleAccessories\":{\"spareBulbs\":true,\"taxDisc\":true},\"technicalFaults\":{\"brakes\":true,\"clutch\":true,\"cooling\":true,\"electrical\":true,\"engine\":true,\"serviceNeeded\":true,\"steering\":true,\"warningLights\":true}},{\"order\":1,\"vehicleCoreData\":{\"unitNumber\":321,\"licensePlate\":\"312\",\"vin\":123,\"carGroup\":\"A\"},\"mileage\":{\"mileage\":22},\"fueling\":{\"fuel\":6},\"damageConditions\":{\"damages\":[{\"area\":\"Front\",\"piece\":\"Bumper\",\"type\":\"Crack\",\"severity\":\"Medium\",\"pictures\":[]}]},\"vehicleAdditionalData\":{\"vehicleType\":\"PKW\",\"model\":\"520\",\"manufacturer\":\"BMW\",\"gearing\":\"manual\",\"fuelType\":\"Diesel\",\"tankCapacity\":33,\"сolour\":\"silver\"},\"overviewPhoto\":{\"pictures\":[]},\"vehicleAccessories\":{},\"technicalFaults\":{}}],\"user\":\"userName\",\"timestamp\":1466427873572,\"uuid\":\"daf5b004-874b-bd01-e2bb-e7c7bddf767d\",\"signature\":{\"signature\":\"data:image/png;base64," + signature + "\"}}";
				UploadContainer uploadContainer = new UploadContainer(url, new UploadUser("test", "test"), json);
				uploadBean.put(getApplicationContext(), uploadContainer);

//			HttpClient httpClient = HttpClientBuilder.create().build(); //Use this instead HttpPut
//				HttpPost request = new HttpPost("http://192.168.81.24:8081/api/v1/infleeting");
//				request.addHeader("content-type", "application/json");
//				//TODO: Collect all stuff from database and put to REST
//				StringEntity params = new StringEntity("{\"deliveryNote\":{\"deliveryNumber\":\"123\",\"licensePlate\":\"31\",\"driverName\":\"123\",\"driverEmail\":\"312@mail.com\",\"vehicleCount\":2,\"condition\":\"ok\"},\"vehicles\":[{\"order\":0,\"vehicleCoreData\":{\"unitNumber\":123,\"licensePlate\":\"31\",\"vin\":123,\"carGroup\":\"A\"},\"mileage\":{\"mileage\":11},\"fueling\":{\"fuel\":2},\"damageConditions\":{\"damages\":[{\"area\":\"Front\",\"piece\":\"Bumper\",\"type\":\"Crack\",\"severity\":\"Small\",\"pictures\":[]}]},\"vehicleAdditionalData\":{\"vehicleType\":\"PKW\",\"model\":\"Q5\",\"manufacturer\":\"Audi\",\"gearing\":\"automatic\",\"fuelType\":\"Diesel\",\"tankCapacity\":22,\"сolour\":\"black\",\"metallic\":true},\"overviewPhoto\":{\"pictures\":[]},\"vehicleAccessories\":{\"spareBulbs\":true,\"taxDisc\":true},\"technicalFaults\":{\"brakes\":true,\"clutch\":true,\"cooling\":true,\"electrical\":true,\"engine\":true,\"serviceNeeded\":true,\"steering\":true,\"warningLights\":true}},{\"order\":1,\"vehicleCoreData\":{\"unitNumber\":321,\"licensePlate\":\"312\",\"vin\":123,\"carGroup\":\"A\"},\"mileage\":{\"mileage\":22},\"fueling\":{\"fuel\":6},\"damageConditions\":{\"damages\":[{\"area\":\"Front\",\"piece\":\"Bumper\",\"type\":\"Crack\",\"severity\":\"Medium\",\"pictures\":[]}]},\"vehicleAdditionalData\":{\"vehicleType\":\"PKW\",\"model\":\"520\",\"manufacturer\":\"BMW\",\"gearing\":\"manual\",\"fuelType\":\"Diesel\",\"tankCapacity\":33,\"сolour\":\"silver\"},\"overviewPhoto\":{\"pictures\":[]},\"vehicleAccessories\":{},\"technicalFaults\":{}}],\"user\":\"userName\",\"timestamp\":1466427873572,\"uuid\":\"daf5b004-874b-bd01-e2bb-e7c7bddf767d\",\"signature\":{\"signature\":\"data:image/png;base64," + signature + "\"}}");
//				request.setEntity(params);
//				HttpResponse response = httpClient.execute(request);
				// handle response here...
			} catch (Exception ex) {
				// handle exception here
				Log.e("TAG", ex.getMessage(), ex);
			}

			return null;

		}

		protected void onPostExecute(Void feed) {
			// TODO: check this.exception
			// TODO: do something with the feed
		}
	}
}
