package checkmobile.de.hertz.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;

import java.io.ByteArrayOutputStream;

import checkmobile.de.hertz.R;

/**
 * Created by berni
 */
public class DrawView extends View {

	private static final int FOREGROUND_COLOR = 0x0;
	private static final int BACKGROUND_COLOR = 0xFFFFFF;
	private static final int ALPHA_ENABLED = 128;
	private static final int SURFACE_ALPHA_DISABLED = 0;

	private Paint drawPaint, backgroundPaint;

	private float lastX, lastY;
	private boolean isDrawing;

	private Bitmap initialBitmap;

	private Bitmap internalBitmap;
	private Canvas internalCanvas;

	public DrawView(Context context) {
		this(context, null);
	}

	public DrawView(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);
		setBackgroundResource(R.drawable.shape_drawview_background);
		backgroundPaint = new Paint();
		drawPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		drawPaint.setStyle(Paint.Style.STROKE);
		drawPaint.setStrokeWidth(3f);
		drawPaint.setColor(FOREGROUND_COLOR);
		drawPaint.setAlpha(255);
		setFocusable(true);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		try {
			internalBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
			internalCanvas = new Canvas(internalBitmap);
			reset();
			if (initialBitmap != null) {
				internalCanvas.drawBitmap(initialBitmap, 0, 0, new Paint());
				invalidate();
				initialBitmap = null;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (internalBitmap != null) {
			backgroundPaint.setAlpha(isEnabled() ? ALPHA_ENABLED : SURFACE_ALPHA_DISABLED);
			canvas.drawBitmap(internalBitmap, 0, 0, backgroundPaint);
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (isEnabled()) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				this.getParent().requestDisallowInterceptTouchEvent(true);
				requestFocus(View.FOCUS_DOWN);
				drawPoint(event.getX(), event.getY(), event.getPressure());
				return true;
			} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
				isDrawing = true;
				drawPoint(event.getX(), event.getY(), event.getPressure());
				return true;
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				this.getParent().requestDisallowInterceptTouchEvent(false);
				isDrawing = false;
				return true;
			}
			return false;
		}
		return super.onTouchEvent(event);
	}

	private void drawPoint(float x, float y, float pressure) {
		if (internalBitmap != null) {
			if (isDrawing) internalCanvas.drawLine(lastX, lastY, x, y, drawPaint);
			else internalCanvas.drawPoint(x, y, drawPaint);
		}
		lastX = x;
		lastY = y;
		invalidate();
	}

	public void reset() {
		if (internalCanvas != null) {
			Paint resetPaint = new Paint();
			resetPaint.setColor(BACKGROUND_COLOR);
			resetPaint.setAlpha(255);
			internalCanvas.drawPaint(resetPaint);
			invalidate();
		}
	}

	public byte[] getPng() {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		internalBitmap.compress(Bitmap.CompressFormat.PNG, 80, os);
		return os.toByteArray();
	}

	public String getAsBase64() {
		return Base64.encodeToString(getPng(), Base64.NO_WRAP);
	}

	public void setBitmap(byte[] data) {
		if (data != null && data.length > 0)
			this.initialBitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
		else reset();
	}

}

