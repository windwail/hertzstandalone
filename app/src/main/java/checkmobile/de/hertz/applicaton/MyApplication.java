package checkmobile.de.hertz.applicaton;

import android.app.Application;

import com.facebook.stetho.Stetho;

import checkmobile.de.hertz.upload.AbstractSyncHelper;
import checkmobile.de.hertz.upload.UploadSyncHelper;

public class MyApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		AbstractSyncHelper syncHelper = new UploadSyncHelper(this);

		if (!syncHelper.checkDefaultSyncSettings(true)) {
			syncHelper.addAccount(true);
		}

		Stetho.initializeWithDefaults(this);
	}

}
