package checkmobile.de.hertz.upload.db;

import android.content.ContentValues;
import android.database.Cursor;

import java.io.File;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UploadDbEntry {

	/** Api based upload. */
	public final static int UPLOAD = 0x1;

	private int type;
	private Date date;
	private File file;
	//private UploadUser user;
	private long id;
	private String host;

	public UploadDbEntry() {}

	public UploadDbEntry(int type, Date date, File file) {
		this.type = type;
		this.date = date;
		this.file = file;
	}

	public UploadDbEntry(Cursor c) throws Exception {
		type = c.getInt(c.getColumnIndex("type"));
		date = new Date(c.getLong(c.getColumnIndex("cdate")));
		file = new File(c.getString(c.getColumnIndex("file")));
		id = c.getLong(c.getColumnIndex("_id"));
	}

	public ContentValues getContentValues() throws Exception {
		ContentValues v = new ContentValues();
		v.put("type", type);
		v.put("cdate", date.getTime());
		v.put("file", file.getAbsolutePath());
		return v;
	}
}
