package checkmobile.de.hertz.upload;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import checkmobile.de.hertz.R;
import checkmobile.de.hertz.helper.NotificationHelper;

public class UploadSyncHelper extends AbstractSyncHelper {

	public final static int DEFAULT_INTERVAL = 7200;

	public final static int RETRY_INTERVAL = 30;

	private final static String NOTIFICATION_TAG = "uploadSyncHelper";
	private final static int NOTIFICATION_ID_SYNC_WARNING = 0x1;

	final static String USER = "hertz_checkmobile";
	final static String PASS = "hertz_pass";

	public UploadSyncHelper(Context context) {
		super(context, context.getString(R.string.sync_authority_upload));
	}

	@Override
	public void addAccount(boolean enableMasterSync) {
		Account account = getSyncAccount();
		if (account == null) {
			account = new Account(USER, context.getString(R.string.sync_account_type));
			AccountManager am = AccountManager.get(context);
			am.addAccountExplicitly(account, PASS, null);
		}
		ContentResolver.addPeriodicSync(account, authority, new Bundle(), 7200);
		if (enableMasterSync) ContentResolver.setMasterSyncAutomatically(true);
		ContentResolver.setIsSyncable(account, authority, 1);
		ContentResolver.setSyncAutomatically(account, authority, true);
		new NotificationHelper(context, NOTIFICATION_TAG).remove(NOTIFICATION_ID_SYNC_WARNING);
	}

	public void removeAccount() {
		Account account = getSyncAccount();
		if (account != null) {
			AccountManager m = AccountManager.get(context);
			m.removeAccount(account, null, null);
		}
	}

	@Override
	public boolean checkDefaultSyncSettings(boolean showNotification) {
		boolean ret = true;
		if (!ContentResolver.getMasterSyncAutomatically()) {
			ret = false;
		}
		Account account = getSyncAccount();
		if (account == null) {
			ret = false;
		}
		if (!ContentResolver.getSyncAutomatically(account, authority)) {
			ret = false;
		}
		if (!ret && showNotification)
			new NotificationHelper(context, NOTIFICATION_TAG).showWarning(NOTIFICATION_ID_SYNC_WARNING, "Sync disabled");
		return ret;
	}

	@Override
	public Account getSyncAccount() {
		AccountManager m = AccountManager.get(context);
		Account[] accounts = m.getAccountsByType(context.getString(R.string.sync_account_type));
		if (accounts != null && accounts.length > 0) {
			return accounts[0];
		}
		return null;
	}

	@Override
	public void performSync() {
		Account account = getSyncAccount();
		Log.d("TAG", "Sync active:" + ContentResolver.isSyncActive(account, authority));
		Log.d("TAG", "Sync pending:" + ContentResolver.isSyncPending(account, authority));
		ContentResolver.requestSync(account, authority, new Bundle());
	}

	@Override
	public void setInterval(int seconds) {
		ContentResolver.addPeriodicSync(getSyncAccount(), authority, new Bundle(), seconds);

	}

}
