package checkmobile.de.hertz.upload;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.io.File;
import java.io.FileWriter;
import java.util.Date;
import java.util.UUID;

import checkmobile.de.hertz.upload.db.UploadDataSource;
import checkmobile.de.hertz.upload.db.UploadDbEntry;
import checkmobile.de.hertz.upload.types.UploadContainer;

@EBean(scope = EBean.Scope.Singleton)
public class UploadBean {

	@Bean
	UploadDataSource uploadDataSource;

	public void put(Context context, UploadContainer container) throws Exception {
		Gson gson = new Gson();
		String uid = UUID.randomUUID().toString();
		File file = new File(context.getFilesDir(), uid + ".json");
		FileWriter writer = new FileWriter(file);
		gson.toJson(container, writer);
		writer.close();
		uploadDataSource.insert(new UploadDbEntry(UploadDbEntry.UPLOAD, new Date(), file));
		new UploadSyncHelper(context.getApplicationContext()).performSync();

	}

	public UploadDbEntry getNextUpload() throws Exception {
		return uploadDataSource.getNextUpload();
	}

	/**
	 * Entfernt einen Upload aus der Datenbank sowie die zugehörigen Dateien aus dem Cache
	 *
	 * @param entry
	 */
	public void removeUpload(UploadDbEntry entry) {
		File f = entry.getFile();
		if (f.exists()) {
			try {
				f.delete();
			} catch (Exception e) {
				Log.e("TAG", "removeUpload", e);
			}
		}
		uploadDataSource.delete(entry.getId());
	}

	/** True, wenn sich keine Pakete im Uploadcache befinden */
	public boolean isEmpty() {
		return uploadDataSource.getCount() == 0;
	}

	/** Returns number of current background upload with the given group */
	public int getGroupUploadCount(String group) {
		return uploadDataSource.getCountByGroup(group);
	}

}
