package checkmobile.de.hertz.upload.service;

import android.accounts.Account;
import android.app.Service;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.google.gson.Gson;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EService;

import java.io.File;
import java.io.FileReader;
import java.util.Date;

import checkmobile.de.hertz.helper.NotificationHelper;
import checkmobile.de.hertz.upload.UploadBean;
import checkmobile.de.hertz.upload.UploadSyncHelper;
import checkmobile.de.hertz.upload.db.UploadDbEntry;
import checkmobile.de.hertz.upload.types.UploadContainer;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;

@EService
public class SyncUploadService extends Service {

	private final static String NOTIFICATION_TAG = "syncUploadService";
	private final static int NOTIFICATION_ID_SYNC = 0x0;
	private final static int NOTIFICATION_ID_ERROR = 0x1;
	private final static Object CREATEADAPTERLOCK = new Object();
	private static SyncAdapterImpl syncAdapter = null;

	@Bean
	UploadBean uploadCache;

	@Override
	public void onCreate() {
		synchronized (CREATEADAPTERLOCK) {
			if (syncAdapter == null) syncAdapter = new SyncAdapterImpl(this);
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return syncAdapter.getSyncAdapterBinder();
	}

	private synchronized void performSync(Context context, Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
		Log.d("TAG", "Upload sync started." + new Date());

		NotificationHelper notificationHelper = new NotificationHelper(context, NOTIFICATION_TAG);
		UploadSyncHelper syncHelper = new UploadSyncHelper(context);
		notificationHelper.remove(NOTIFICATION_ID_ERROR);

		notificationHelper.showSync(NOTIFICATION_ID_SYNC, "Synchronizing tasks");

		boolean syncSuccess = true;
		try {
			UploadDbEntry upload;
			while ((upload = uploadCache.getNextUpload()) != null) {
				switch (upload.getType()) {
					case UploadDbEntry.UPLOAD: {
						Log.d("TAG", "Upload:");
						File uploadFile = upload.getFile();
						if (uploadFile.exists()) {
							Log.d("TAG", "Deserializing " + upload.getFile() + " Size: " + upload.getFile().length());
							Gson gson = new Gson();
							FileReader reader = new FileReader(upload.getFile());
							UploadContainer uploadContainer = gson.fromJson(reader, UploadContainer.class);
							//
							if (uploadContainer != null) {
								Log.d("TAG", "Uploading...");
								HttpClient httpClient = HttpClientBuilder.create().build(); //Use this instead HttpPut
								HttpPost request = new HttpPost(uploadContainer.getUrl());
								request.setHeader("Content-Type", "application/json ");
								StringEntity params = new StringEntity(uploadContainer.getContent());
								request.setEntity(params);
								HttpResponse response = httpClient.execute(request);
								// handle response here...
								StatusLine statusLine = response.getStatusLine();
								switch (statusLine.getStatusCode()) {
									case 200: { }
									break;
									default: {
										throw new Exception("Upload error: Status " + statusLine.getStatusCode() + " " + statusLine.getReasonPhrase());
									}
								}
							}

						} else {
							Log.d("TAG", "SKIP: Upload file does not exist.");
						}
					}
					break;
					default:
						Log.d("TAG", "Unknown upload type " + upload.getType());
						break;
				}
				// Ende Switch
				Log.d("TAG", "Removing upload...");
				uploadCache.removeUpload(upload);
				Log.d("TAG", "Upload removed.");
			}
		} catch (Exception e1) {
			syncSuccess = false;
			Log.e("TAG", "Sync error", e1);
			notificationHelper.showWarning(NOTIFICATION_ID_ERROR, "Sync failed. " + e1.getMessage() );
		}

		if (syncSuccess) syncHelper.setInterval(UploadSyncHelper.DEFAULT_INTERVAL);
		else syncHelper.setInterval(UploadSyncHelper.RETRY_INTERVAL);
		//
		notificationHelper.remove(NOTIFICATION_ID_SYNC);
		Log.d("TAG", "Sync completed." + new Date());
	}

	private class SyncAdapterImpl extends AbstractThreadedSyncAdapter {

		private Context context;

		public SyncAdapterImpl(Context context) {
			super(context, true);
			this.context = context;
		}

		@Override
		public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
			performSync(context, account, extras, authority, provider, syncResult);
		}
	}
}
