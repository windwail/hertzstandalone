package checkmobile.de.hertz.upload.types;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UploadUser {

	String login;
	String pass;

	public UploadUser() { }

	public UploadUser(String login, String pass) {
		this.login = login;
		this.pass = pass;
	}
}