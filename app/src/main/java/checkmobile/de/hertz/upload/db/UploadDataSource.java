package checkmobile.de.hertz.upload.db;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils.InsertHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;

@EBean(scope = Scope.Singleton)
public class UploadDataSource {

	private SQLiteDatabase database;
	private UploadSyncScheme uploadScheme;

	public UploadDataSource(Context context) {
		uploadScheme = new UploadSyncScheme(context);
	}

	public boolean insert(UploadDbEntry entry) throws Exception {
		try {
			open();
			database.beginTransaction();
			InsertHelper ih = new InsertHelper(database, UploadSyncScheme.TABLEUPLOAD);
			// Insert mit Contenvalues ist threadsicher!
			ih.insert(entry.getContentValues());
			database.setTransactionSuccessful();
		} finally {
			database.endTransaction();
		}
		return true;
	}

	public void deleteAll() {
		open();
		database.beginTransaction();
		try {
			database.execSQL("DELETE FROM " + UploadSyncScheme.TABLEUPLOAD);
			database.setTransactionSuccessful();
		} finally {
			database.endTransaction();
		}
	}

	public void delete(long id) {
		open();
		database.beginTransaction();
		try {
			database.execSQL(String.format("DELETE FROM %s WHERE %s=%d", UploadSyncScheme.TABLEUPLOAD, "_id", id));
			database.setTransactionSuccessful();
		} finally {
			database.endTransaction();
		}
	}

	public int getCount() {
		open();
		int ret = 0;
		Cursor c = database.rawQuery(String.format("SELECT count(*) FROM %s", UploadSyncScheme.TABLEUPLOAD), null);
		if (c.moveToFirst()) {
			ret = c.getInt(0);
		}
		closeCursor(c);
		return ret;
	}

	public int getCountByGroup(String uploadGroup) {
		open();
		int ret = 0;
		Cursor c = database.rawQuery(String.format("SELECT count(*) FROM %s WHERE uploadgroup='%s'", UploadSyncScheme.TABLEUPLOAD, uploadGroup), null);
		if (c.moveToFirst()) {
			ret = c.getInt(0);
		}
		closeCursor(c);
		return ret;
	}

	public UploadDbEntry getNextUpload() throws Exception {
		open();
		UploadDbEntry ret = null;
		Cursor c = database.rawQuery(String.format("SELECT * FROM %s order by _id", UploadSyncScheme.TABLEUPLOAD), null);
		if (c.moveToFirst()) {
			ret = new UploadDbEntry(c);
		}
		closeCursor(c);
		return ret;
	}

	public void open() {
		if (database == null || !database.isOpen()) database = uploadScheme.getWritableDatabase();
	}

	public void close() {
		database.close();
	}

	private void closeCursor(Cursor c) {
		try {
			c.close();
		} catch (Exception e) {
			Log.e("TAG", "closeCursor", e);
		}
	}
}
