package checkmobile.de.hertz.upload.types;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UploadContainer {

	String url;
	String content;
	UploadUser user;

	public UploadContainer() {}

	public UploadContainer(String url, UploadUser user, String content) {
		this.url = url;
		this.user = user;
		this.content = content;
	}
}
