package checkmobile.de.hertz.upload.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class UploadSyncScheme extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "upload.db";
	public static final int DATABASE_VERSION = 1;
	public static final String TABLEUPLOAD = "upload";

	private static final String CREATE_TABLE_UPLOAD = "CREATE TABLE  " + TABLEUPLOAD + "(" + //
			"_id INTEGER PRIMARY KEY AUTOINCREMENT," + //
			" cdate TIMESTAMP NOT NULL DEFAULT current_timestamp," + //
			" file TEXT NOT NULL," + //
			" type INTEGER NOT NULL);";

	public UploadSyncScheme(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		Log.d("TAG", "Creating database...");
		database.execSQL(CREATE_TABLE_UPLOAD);
		Log.d("TAG", "Database created.");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.d("TAG", "Upgrading database");
		db.execSQL("DROP TABLE IF EXISTS " + TABLEUPLOAD);
		onCreate(db);
		Log.d("TAG", "Upgrade finished.");
	}
}
