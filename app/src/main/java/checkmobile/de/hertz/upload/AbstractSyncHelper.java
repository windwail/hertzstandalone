package checkmobile.de.hertz.upload;

import android.accounts.Account;
import android.content.Context;

import lombok.Getter;

public abstract class AbstractSyncHelper {

	@Getter
	protected Context context;

	@Getter
	protected String authority;

	public AbstractSyncHelper(Context context, int authority) {
		this(context, context.getString(authority));
	}

	public AbstractSyncHelper(Context context, String authority) {
		this.context = context;
		this.authority = authority;
	}

	public abstract void addAccount(boolean enableMasterSync);

	public abstract void removeAccount();

	public abstract boolean checkDefaultSyncSettings(boolean showNotification);

	public abstract Account getSyncAccount();

	public abstract void performSync();

	public abstract void setInterval(int seconds);

}