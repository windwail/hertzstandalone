package checkmobile.de.hertz.helper;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import checkmobile.de.hertz.R;

/**
 * Helper class to show Notifications
 *
 * @author berni
 */
public class NotificationHelper {

	private String tag;
	private Context context;
	private NotificationManager notificationManager;

	public NotificationHelper(Context context, String tag) {
		this.context = context;
		this.tag = tag;
		this.notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
	}

	/**
	 * Zeigt eine Warnung in der Statusleiste an
	 *
	 * @param id   Eine eindeutige id. Kann zum entfernen verwendet werden.
	 * @param text Der TExt. Wird momentan nicht übersetzt.
	 */
	public void showWarning(int id, String text) {
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		new NotificationCompat.Builder(context);

		builder.setSmallIcon(R.drawable.icon_warn);
		builder.setContentInfo("Warning");
		builder.setContentTitle(context.getString(R.string.app_name));
		builder.setStyle(new NotificationCompat.BigTextStyle().bigText(text));
		//builder.setContentText(String.format("%s\n%s", text, new Date()));
		builder.setContentText(text);
		Notification n = builder.build();
		n.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(tag, id, n);
	}

	public void showSync(int id, String text) {
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		builder.setSmallIcon(R.drawable.icon_sync);
		builder.setContentInfo("Sync");
		builder.setStyle(new NotificationCompat.BigTextStyle().bigText(text));
		builder.setContentTitle(context.getString(R.string.app_name));
		builder.setContentText(text);
		Notification n = builder.build();
		n.flags |= Notification.FLAG_ONGOING_EVENT;
		notificationManager.notify(tag, id, n);
	}

	public void remove(int id) {
		notificationManager.cancel(tag, id);
	}

}
